#!/bin/bash

docker pull mongo;
docker run -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME='root' -e MONGO_INITDB_ROOT_PASSWORD='root' -e MONGO_INITDB_DATABASE='mongo' --name mongodb-chat mongo;
