package br.com.edu.ifpb.backend.handleException;

import br.com.edu.ifpb.backend.handleException.Exceptions.ExistingConversationException;
import br.com.edu.ifpb.backend.handleException.Exceptions.ExistingUserException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(ExistingConversationException.class)
    public ResponseEntity handleExistingConversationException(ExistingConversationException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(e.getMessage());
    }

    @ExceptionHandler(ExistingUserException.class)
    public ResponseEntity handleExistingUserException(ExistingUserException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(e.getMessage());
    }
}