package br.com.edu.ifpb.backend.service;

import br.com.edu.ifpb.backend.model.Message;
import br.com.edu.ifpb.backend.model.Messages;
import br.com.edu.ifpb.backend.repository.MessagesRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessagesService {
    private MessagesRepository messagesRepository;

    public Messages create(Messages messages){
        return messagesRepository.save(messages);
    }

    public void addNewMessage(Message message, String conversationId) {
        Messages messages = messagesRepository.findById(conversationId);
        messages.addMessage(message);
        messagesRepository.save(messages);
    }

    public List<Message> listConversationMessage(String conversation) {
        Messages messages = messagesRepository.findById(conversation);
        return messages.getMessageList();
    }
}
