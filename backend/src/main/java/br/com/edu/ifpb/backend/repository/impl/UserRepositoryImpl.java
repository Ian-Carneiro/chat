package br.com.edu.ifpb.backend.repository.impl;

import br.com.edu.ifpb.backend.model.User;
import br.com.edu.ifpb.backend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private MongoTemplate mongoTemplate;

    @Override
    public String findByEmailJustReturningTopicId(String email) {
        Query q = new Query();
        q.fields().include("topicId").exclude("_id");
        q.addCriteria(Criteria.where("_id").is(email));
        HashMap topicId = mongoTemplate.findOne(q, HashMap.class, "users");
        if(topicId!=null){
            return topicId.get("topicId").toString();
        }
        return null;
    }

    @Override
    public List<String> findByEmailJustReturningTopicId(List<String> email) {
        Query q = new Query();
        q.fields().include("topicId").exclude("_id");
        q.addCriteria(Criteria.where("_id").in(email));
        return mongoTemplate.find(q, HashMap.class, "users")
                .stream().filter(Objects::nonNull)
                .map(t -> t.get("topicId").toString()).collect(Collectors.toList());
    }

    @Override
    public User save(User user) {
        return mongoTemplate.save(user);
    }

    @Override
    public Optional<User> findById(String id) {
        return Optional.ofNullable(mongoTemplate.findById(id, User.class));
    }

    @Override
    public boolean existsById(String id) {
        return mongoTemplate.exists(Query.query(Criteria.where("_id").is(id)), User.class);
    }

    @Override
    public List<User> findAll() {
        return mongoTemplate.findAll(User.class);
    }

    @Override
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
