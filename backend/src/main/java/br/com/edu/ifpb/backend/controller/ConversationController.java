package br.com.edu.ifpb.backend.controller;

import br.com.edu.ifpb.backend.model.Conversation;
import br.com.edu.ifpb.backend.model.Message;
import br.com.edu.ifpb.backend.service.MessagesService;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class ConversationController {
    private SimpMessageSendingOperations messagingTemplate;
    private MessagesService messagesService;

    @MessageMapping("/chat.sendConversation")
    @SendTo("/topic/user/{topicId}")
    public Conversation send(@Payload Conversation conversation, @DestinationVariable String topicId) {
        return conversation;
    }

    public void convertAndSendConversation(Conversation conversation, String topicId){
        messagingTemplate.convertAndSend("/topic/user/"+topicId, conversation);
    }

    @MessageMapping("/chat.sendMessage/{conversation}")
    @SendTo("/topic/conversation/{conversation}")
    public Message sendMessage(@Payload Message message, @DestinationVariable("conversation") String conversation) {
        messagesService.addNewMessage(message, conversation);
        return message;
    }

}
