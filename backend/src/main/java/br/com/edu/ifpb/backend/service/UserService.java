package br.com.edu.ifpb.backend.service;

import br.com.edu.ifpb.backend.handleException.Exceptions.ExistingUserException;
import br.com.edu.ifpb.backend.model.User;
import br.com.edu.ifpb.backend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;


    public User create(User user) throws ExistingUserException {
        if (existsUser(user.getEmail())){
            throw new ExistingUserException();
        }
        return userRepository.save(user);
    }

    public List<User> list(){
        return userRepository.findAll();
    }

    public Optional<User> getUserByEmail(String email) {
        return userRepository.findById(email);
    }


    public List<String> getTopicIdByEmail(List<String> emails) {
        List<String> idList = userRepository.findByEmailJustReturningTopicId(emails);
        return idList;
    }

    public void addConversation(String idConversation, String email) {
        User user = userRepository.findById(email).get();
        user.addConversation(idConversation);
        userRepository.save(user);
    }

    public Boolean existsUser(String email) {
        return userRepository.existsById(email);
    }
}
