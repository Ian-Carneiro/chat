package br.com.edu.ifpb.backend.repository;

import org.springframework.data.mongodb.core.MongoTemplate;

public interface SetTemplate {
    void setMongoTemplate(MongoTemplate mongoTemplate);
}
