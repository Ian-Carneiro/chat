package br.com.edu.ifpb.backend.repository;

import br.com.edu.ifpb.backend.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends SetTemplate{

    String findByEmailJustReturningTopicId(String email);

    List<String> findByEmailJustReturningTopicId(List<String> email);

    User save(User user);

    Optional<User> findById(String id);

    boolean existsById(String id);

    List<User> findAll();
}
