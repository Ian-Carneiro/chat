package br.com.edu.ifpb.backend.controller;


import br.com.edu.ifpb.backend.handleException.Exceptions.ExistingConversationException;
import br.com.edu.ifpb.backend.model.Conversation;
import br.com.edu.ifpb.backend.model.Ids;
import br.com.edu.ifpb.backend.model.Message;
import br.com.edu.ifpb.backend.service.ConversationService;
import br.com.edu.ifpb.backend.service.MessagesService;
import br.com.edu.ifpb.backend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/conversation")
@AllArgsConstructor
public class ConversationRestController {
    private ConversationService conversationService;
    private UserService userService;
    private ConversationController conversationController;
    private MessagesService messagesService;

    @PostMapping("")
    public ResponseEntity<Conversation> create(@RequestBody Conversation conversation) throws ExistingConversationException {
        Conversation c = conversationService.create(conversation);
        for (String email: conversation.getParticipants()){
            userService.addConversation(c.getId(), email);
        }
        for (String topic: userService.getTopicIdByEmail(conversation.getParticipants())){
            conversationController.convertAndSendConversation(c, topic);
        }
        return ResponseEntity.status(200).body(c);
    }

    @PostMapping("/list")
    public ResponseEntity<Iterable<Conversation>> listConversationsById(@RequestBody Ids ids){
        Iterable<Conversation> conversations = conversationService.listConversationsById(ids.getIds());
        return ResponseEntity.status(200).body(conversations);
    }

    @GetMapping("/{conversation}/messages")
    public ResponseEntity<List<Message>> listConversationMessage(@PathVariable String conversation) {
        List<Message> messages = messagesService.listConversationMessage(conversation);
        return ResponseEntity.status(200).body(messages);
    }
}
