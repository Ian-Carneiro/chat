package br.com.edu.ifpb.backend.repository;

import br.com.edu.ifpb.backend.model.Message;
import br.com.edu.ifpb.backend.model.Messages;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessagesRepository extends SetTemplate {

    Messages save(Messages messages);

    Messages findById(String conversationId);

}
