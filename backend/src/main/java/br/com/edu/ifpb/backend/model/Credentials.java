package br.com.edu.ifpb.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Credentials {
    private String user;
    private String pswd;
    private String host;
    private String dbname;
    private String drive;

    public static boolean isUpdated = false;
}
