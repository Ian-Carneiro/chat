package br.com.edu.ifpb.backend.repository.impl;

import br.com.edu.ifpb.backend.model.Messages;
import br.com.edu.ifpb.backend.repository.MessagesRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class MessagesRepositoryImpl implements MessagesRepository {

    private MongoTemplate mongoTemplate;

    @Override
    public Messages save(Messages messages) {
        return mongoTemplate.save(messages);
    }

    @Override
    public Messages findById(String conversationId) {
        return mongoTemplate.findById(conversationId, Messages.class);
    }


    @Override
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
