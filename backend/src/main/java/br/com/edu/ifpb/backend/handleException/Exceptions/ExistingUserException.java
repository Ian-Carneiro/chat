package br.com.edu.ifpb.backend.handleException.Exceptions;

public class ExistingUserException extends Exception {
    public ExistingUserException(){
        super("Já existe um cadastro com este e-mail.");
    }
}
