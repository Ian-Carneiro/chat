package br.com.edu.ifpb.backend.config;

import br.com.edu.ifpb.backend.client.CredentialsController;
import br.com.edu.ifpb.backend.model.Credentials;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Objects;

@Configuration
@EnableMongoRepositories({"br.com.edu.ifpb.backend.repository"})
public class DatabaseConfiguration {

    private CredentialsController credentialsController;
    public DatabaseConfiguration(CredentialsController credentialsController){
        this.credentialsController = credentialsController;
    }

    private Credentials credentials;

    private MongoClientFactoryBean currentMongoClientFactoryBean = null;

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MongoClientFactoryBean mongo() throws Exception {
        if (currentMongoClientFactoryBean != null && !Credentials.isUpdated) {
            currentMongoClientFactoryBean.destroy();
            currentMongoClientFactoryBean = null;
        }
        if (!Credentials.isUpdated){
            credentials = credentialsController.getCredentials();
            Credentials.isUpdated = true;
        }
        MongoClientFactoryBean mongo = new MongoClientFactoryBean();
        String[] host = credentials.getHost().split(":");
        mongo.setMongoClientSettings(MongoClientSettings.builder().build());
        mongo.setHost(host[0]);
        mongo.setPort(Integer.parseInt(host[1]));
        mongo.setCredential(new MongoCredential[]{MongoCredential.createScramSha1Credential(
                credentials.getUser(), "admin", credentials.getPswd().toCharArray())});
        this.currentMongoClientFactoryBean = mongo;
        return mongo;
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MongoTemplate mongoTemplate() throws Exception{
        if (Credentials.isUpdated)
            return new MongoTemplate(Objects.requireNonNull(currentMongoClientFactoryBean.getObject()), credentials.getDbname());
        return new MongoTemplate(Objects.requireNonNull(mongo().getObject()), credentials.getDbname());
    }

}