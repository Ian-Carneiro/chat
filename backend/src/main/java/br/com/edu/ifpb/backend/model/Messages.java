package br.com.edu.ifpb.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document(collection = "messages")
public class Messages {
    @Id
    private String conversationId;
    private List<Message> messageList;

    public void addMessage(Message m) {
        messageList.add(m);
    }
}
