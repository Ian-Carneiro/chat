package br.com.edu.ifpb.backend.repository.impl;

import br.com.edu.ifpb.backend.model.Conversation;
import br.com.edu.ifpb.backend.repository.ConversationRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@AllArgsConstructor
@Component
public class ConversationRepositoryImpl implements ConversationRepository {

    private MongoTemplate mongoTemplate;

    @Override
    public boolean existsByParticipants(List<String> participants) {
        return mongoTemplate.exists(Query.query(Criteria.where("participants").is(participants)), Conversation.class);
    }

    @Override
    public Conversation save(Conversation conversation) {
        return mongoTemplate.save(conversation);
    }

    @Override
    public List<Conversation> findAllById(List<String> ids) {
        return mongoTemplate.find(Query.query(Criteria.where("_id").in(ids)), Conversation.class);
    }

    @Override
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
