package br.com.edu.ifpb.backend.client;


import br.com.edu.ifpb.backend.model.Credentials;
import br.com.edu.ifpb.backend.model.CredentialsServiceResponse;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Objects;

@Controller
public class CredentialsController {
    @Autowired
    private WebClient webClient;

    @Value("${credentials.service.request.token}")
    private String token;

    public Credentials getCredentials() {
        try {
            CredentialsServiceResponse credentialsServiceResponse = Objects.requireNonNull(webClient.get()
                    .uri("/credentials?token=" + token)
//                .exchange();
                    .retrieve()
                    .bodyToMono(CredentialsServiceResponse.class).block());
            if (credentialsServiceResponse.isSuccess()) {
                return credentialsServiceResponse.getData();
            } else {
                LoggerFactory.getLogger(this.getClass().getName()).warn(credentialsServiceResponse.getError() + " - " + credentialsServiceResponse.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LoggerFactory.getLogger(this.getClass().getName()).error("Fail on request Credential API");
        }
        return new Credentials();
    }

}