package br.com.edu.ifpb.backend.handleException.Exceptions;

public class ExistingConversationException extends Exception {
    public ExistingConversationException(){
        super("Já existe uma conversa particular com este usuário");
    }
}
