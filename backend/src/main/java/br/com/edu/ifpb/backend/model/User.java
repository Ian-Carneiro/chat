package br.com.edu.ifpb.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.*;

@NoArgsConstructor
@Getter
@Setter
@Document(collection = "users")
public class User implements UserDetails {
    @Id
    private String email;
    private String password;
    private String name;
    private List<String> conversations = new ArrayList<>();
    private String topicId = UUID.randomUUID().toString();

    public User(String topicId){
        this.topicId = topicId;
    }

    public void addConversation(String idConversation){
        conversations.add(idConversation);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("USER"));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
