package br.com.edu.ifpb.backend.config;

import br.com.edu.ifpb.backend.model.Credentials;
import br.com.edu.ifpb.backend.repository.ConversationRepository;
import br.com.edu.ifpb.backend.repository.MessagesRepository;
import br.com.edu.ifpb.backend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Scheduled;


@Configuration
@AllArgsConstructor
public class RefreshMongoTemplate {

    private UserRepository userRepository;
    private ConversationRepository conversationRepository;
    private MessagesRepository messagesRepository;

    @Scheduled(fixedRate = 1000*60, initialDelay = 1000*60)
    public void refresh() {
        Credentials.isUpdated = false;
        MongoTemplate mongoTemplate = getMongoTemplate();
        userRepository.setMongoTemplate(mongoTemplate);
        conversationRepository.setMongoTemplate(mongoTemplate);
        messagesRepository.setMongoTemplate(mongoTemplate);
    }

    @Lookup
    public MongoTemplate getMongoTemplate() {
        return null;
    }
}
