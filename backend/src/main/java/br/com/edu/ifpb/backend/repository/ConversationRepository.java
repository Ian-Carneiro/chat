package br.com.edu.ifpb.backend.repository;

import br.com.edu.ifpb.backend.model.Conversation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConversationRepository extends SetTemplate {
    boolean existsByParticipants(List<String> participants);
    Conversation save(Conversation conversation);
    List<Conversation> findAllById(List<String> ids);
}
