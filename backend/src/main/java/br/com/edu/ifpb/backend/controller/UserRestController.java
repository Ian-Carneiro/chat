package br.com.edu.ifpb.backend.controller;

import br.com.edu.ifpb.backend.handleException.Exceptions.ExistingUserException;
import br.com.edu.ifpb.backend.model.User;
import br.com.edu.ifpb.backend.model.UserDTO;
import br.com.edu.ifpb.backend.service.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserRestController {
    private UserService userService;
    private final BCryptPasswordEncoder passwordEncoder;

//    @GetMapping("")
//    public ResponseEntity<List<User>> list(){
//        List<User> users = userService.list();
//        return ResponseEntity.status(200).body(users);
//    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> create(@RequestBody User user) throws ExistingUserException {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User u = userService.create(user);
        return ResponseEntity.ok(
                new UserDTO(
                        u.getEmail(),
                        u.getName(),
                        u.getConversations(),
                        u.getTopicId()
                )
        );
    }

//    @GetMapping("/{email}")
//    public ResponseEntity<User> getUserByEmail(@PathVariable String email){
//        User user = userService.getUserByEmail(email).  get();
//        return ResponseEntity.status(200).body(user);
//    }

    @GetMapping("/exists/{email}")
    public ResponseEntity existsUser(@PathVariable String email){
        Boolean exists = userService.existsUser(email);
        return ResponseEntity.status(200).body(new HashMap<String, Boolean>(){{
            put("exists", exists);
        }});
    }

    @PostMapping("/login")
    public ResponseEntity authenticate(@RequestBody User user) {
        try {
            User matchedUser = userService.getUserByEmail(user.getEmail()).get();
            if (passwordEncoder.matches(user.getPassword(), matchedUser.getPassword())) {
                return ResponseEntity.ok(
                        new UserDTO(
                                matchedUser.getEmail(),
                                matchedUser.getName(),
                                matchedUser.getConversations(),
                                matchedUser.getTopicId()
                        )
                );
            }
            return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            e.printStackTrace();
            LoggerFactory.getLogger(getClass().getName()).warn("User not found for: " + user.getEmail());
            return ResponseEntity.notFound().build();
        }
    }


}
