package br.com.edu.ifpb.backend.service;

import br.com.edu.ifpb.backend.handleException.Exceptions.ExistingConversationException;
import br.com.edu.ifpb.backend.model.Conversation;
import br.com.edu.ifpb.backend.model.Messages;
import br.com.edu.ifpb.backend.repository.ConversationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ConversationService {
    private ConversationRepository conversationRepository;
    private MessagesService messagesService;

    public boolean existsByParticipants(List<String> participants){
        return conversationRepository.existsByParticipants(participants);
    }

    public Conversation create(Conversation conversation) throws ExistingConversationException{
        if(conversation.getParticipants().size()==2 && existsByParticipants(conversation.getParticipants())){
            throw new ExistingConversationException();
        }
        Conversation saved = conversationRepository.save(conversation);
        messagesService.create(new Messages(saved.getId(), new ArrayList<>()));
        return saved;
    }

    public Iterable<Conversation> listConversationsById(List<String> ids) {
        return conversationRepository.findAllById(ids);
    }
}
