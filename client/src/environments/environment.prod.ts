export const environment = {
  production: true,
  apiUrlBase: "http://chat_backend:API_PORT",
  webSocketEndPoint: "http://chat_backend:API_PORT/ws",
  topicBase: "/topic/",
  aplicationDestinationBase: "/app/" 
};
