export class Message {
  constructor(
    public time: Date,
    public content: string,
    public senderName: string,
    public email: string
  ) {}
}