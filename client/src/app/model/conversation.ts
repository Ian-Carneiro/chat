export class Conversation{
    constructor(public name:string, public participants: string[], public id?: string, public lastUpdated?: Date, public viewed?: boolean, public selected?: boolean){}
}