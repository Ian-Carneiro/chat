export class UserSession {
  constructor(
    public email: string,
    public name: string,
    public conversations: string[],
    public topicId: string
    ) { }
}
