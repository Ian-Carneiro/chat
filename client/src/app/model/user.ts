export class User{
    constructor(public email:string, public password:string, public name:string, public conversations?: string[], public topicId?: string){}
}