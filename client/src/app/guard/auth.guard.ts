import { EncryptionService } from './../services/encryption.service';
import { LocalStorageService } from './../services/local-storage.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { UserSession } from '../model/user-session';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private localStorage: LocalStorageService,
    private router: Router,
    private encryptService: EncryptionService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const data = this.localStorage.get('user');
    if (data) {
      const user: UserSession = data;
      if (user) {
        const cipherText = this.localStorage.get('password');
        const passwordPlain = this.encryptService.decrypt(cipherText, user.email);
        if (passwordPlain) {
          return true;
        }
      }
    }
    this.router.navigate(['/login']);
    return false;
  }

}
