import { Injectable } from '@angular/core';
import * as Crypto from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncryptionService {

  private salt: string = 'f1nd1ngn3m0';

  constructor() { }

  encrypt(password: string, email: string) {
    return Crypto.AES.encrypt(password, email + this.salt).toString();
  }

  decrypt(encryptedData, keyEmail: string) {
    const bytes = Crypto.AES.decrypt(encryptedData, keyEmail + this.salt);
    return bytes.toString(Crypto.enc.Utf8);
  }
}
