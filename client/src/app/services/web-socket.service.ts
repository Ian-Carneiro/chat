import { environment } from './../../environments/environment';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

// @Injectable({
//   providedIn: 'root'
// })
export class WebSocketService {

    stompClient: any;
    handlerResponse: Function;
    handlerResponseError: Function;

    constructor(private paramId: string, private topic: string, private handleResponse?: Function, private handleResponseError?: Function){
      this.handleResponse = handleResponse || this.onMessageReceived;
      this.handleResponseError = handleResponseError || this.errorCallBack;
    }

    _connect() {
        console.log("Initialize WebSocket Connection");
        let ws = new SockJS(environment.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function (frame) {
            _this.stompClient.subscribe(environment.topicBase+_this.topic, function (sdkEvent) {
                _this.handleResponse(JSON.parse(sdkEvent.body), _this.paramId);
            });
        }, this.handleResponseError);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    errorCallBack(error) {
        console.log("errorCallBack -> " + error)
        setTimeout(() => {
            this._connect();
        }, 5000);
    }

    onMessageReceived(message) {
      console.log("Message Recieved from Server :: " + message);
    }

    _send(message, aplicationDestination: string) {
        console.log('message', message);
        this.stompClient.send(environment.aplicationDestinationBase+aplicationDestination,
          {}, JSON.stringify(message));
    }
}
