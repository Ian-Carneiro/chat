import { LocalStorageService } from './local-storage.service';
import { EncryptionService } from './encryption.service';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { UserSession } from '../model/user-session';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(
    private encryptionService: EncryptionService,
    private localStorageService: LocalStorageService
  ) { }

  buildHeaderAuth(): HttpHeaders {
    let header: HttpHeaders = new HttpHeaders();
    const data = this.localStorageService.get('user');
    if (data) {
      const user: UserSession = data;
      if (user) {
        const cipherText = this.localStorageService.get('password');
        const passwordPlain = this.encryptionService.decrypt(cipherText, user.email);
        if (passwordPlain) {
          header = header.append('authorization', `Basic ${btoa(user.email + ':' + passwordPlain)}`);
        }
      }
    }
    console.log('header', header);
    return header;
  }
}
