import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Message } from '../model/message';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) { }

  listConversationMessages(conversationId: string): Promise<Array<Message>> {
    return this.http.get<Array<Message>>(
      `${environment.apiUrlBase}conversation/${conversationId}/messages`,
      { 
        headers: this.configService.buildHeaderAuth()
      }
    ).toPromise();
  }
}
