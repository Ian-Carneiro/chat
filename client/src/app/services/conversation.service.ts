import { ConfigService } from './config.service';
import { Conversation } from './../model/conversation';
import { environment } from './../../environments/environment';
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {

  static emitConversationId = new EventEmitter<string>();
  static emitConversationIdToRenderMessages = new EventEmitter<string>();

  constructor(
    private http: HttpClient,
    private configService: ConfigService
  ) { }

  listConversations(ids: string[]){
    return this.http.post<Conversation[]>(
      environment.apiUrlBase+"conversation/list",
      {ids},
      { 
        headers: this.configService.buildHeaderAuth()
      }
      ).toPromise();
  }

  createConversation(conversation: Conversation):Promise<Conversation>{
    return this.http.post<Conversation>(
      environment.apiUrlBase+"conversation", 
      conversation,
      { headers: this.configService.buildHeaderAuth() }
      ).toPromise();
  }
}
