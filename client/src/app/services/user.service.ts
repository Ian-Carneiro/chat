import { User } from './../model/user';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSession } from '../model/user-session';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private http: HttpClient, private configService: ConfigService) { }

  // login(email: string){
  //   return this.http.get<User>(environment.apiUrlBase+`user/${email}`).toPromise();
  // }

  signup(user: User): Promise<UserSession>{
    return this.http.post<UserSession>(environment.apiUrlBase+`user/register`, user).toPromise();
  }

  exists(email: string){
    return this.http.get(
      environment.apiUrlBase+`user/exists/${email}`,
      { 
        headers: this.configService.buildHeaderAuth()
      }
    ).toPromise();
  }

  authenticate(email: string, password: string): Promise<UserSession> {
    return this.http.post<UserSession>(environment.apiUrlBase + 'user/login', { email, password }).toPromise();
  }
}
