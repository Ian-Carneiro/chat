import { Injectable } from '@angular/core';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private storage: Storage;

  constructor() {
    this.storage = window.localStorage;
  }

  set(key: string, value: any) {
    this.storage.setItem(key, JSON.stringify(value));
  }

  addConversationId(key: string, id: string) {
    let user: User = this.get(key);
    user.conversations.unshift(id);
    this.storage.setItem(key, JSON.stringify(user));
  }

  get(key: string){
    return JSON.parse(localStorage.getItem(key));
  }

  remove(key: string){
    localStorage.removeItem(key);
  }


}
