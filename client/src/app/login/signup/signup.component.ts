import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user';
import { UserSession } from 'src/app/model/user-session';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  name: string = '';
  email: string = '';
  password: string = '';

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  register(){
    if (this.name==''){
      console.log('informe o seu nome.')
      return     
    }
    if (this.email.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')==null){
      console.log('informe um e-mail válido.')
      return     
    }
    if (this.password==''){
      console.log('informe o sua senha.') 
      return    
    }

    this.userService.signup(new User(this.email, this.password, this.name)).then((res: UserSession)=>{
      this.router.navigate(['/'])
    }).catch((err: HttpErrorResponse)=>{
      console.log(err.error);
    });
  }
}
