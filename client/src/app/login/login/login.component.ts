import { EncryptionService } from './../../services/encryption.service';
import { LocalStorageService } from './../../services/local-storage.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserSession } from 'src/app/model/user-session';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string = 'ian@gmail.com';
  password: string = '1234567890';

  constructor(
    private userService:UserService,
    private localStorageService:LocalStorageService,
    private router: Router,
    private encryptionService: EncryptionService
  ) { }

  ngOnInit() {
  }

  login() {
    this.userService.authenticate(this.email, this.password).then((user: UserSession) => {
      this.localStorageService.set('user', user);
      this.router.navigate(['/chat']);
      // encrypt password
      const ciphertext = this.encryptionService.encrypt(this.password, this.email);
      this.localStorageService.set('password', ciphertext);

    }).catch(err => {
      console.log('fail on authenticate', err);
    });
  }

  signup(){
    this.router.navigate(['/signup'])
  }

}
