import { LocalStorageService } from 'src/app/services/local-storage.service';
import { User } from './../../model/user';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chat-screen',
  templateUrl: './chat-screen.component.html',
  styleUrls: ['./chat-screen.component.scss']
})
export class ChatScreenComponent implements OnInit {
  user:User
  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.user = this.localStorageService.get('user');
  }


}
