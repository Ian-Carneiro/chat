import { LocalStorageService } from 'src/app/services/local-storage.service';
import { User } from 'src/app/model/user';
import { MessagesService } from './../../services/messages.service';
import { ConversationService } from './../../services/conversation.service';
import { Component, OnInit, Input } from '@angular/core';
import { WebSocketService } from 'src/app/services/web-socket.service';
import { Message } from 'src/app/model/message';

@Component({
  selector: 'app-message-area',
  templateUrl: './message-area.component.html',
  styleUrls: ['./message-area.component.scss']
})
export class MessageAreaComponent implements OnInit {
  
  @Input()
  conversationsId: string[]
  webSocket: WebSocketService;

  hasTopic: boolean = false;
  messageText: string = '';

  messages: Message[] = new Array();

  user: User;

  topic: string;

  constructor(
    private messagesService: MessagesService,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit() {
    this.getUser();
    ConversationService.emitConversationIdToRenderMessages.subscribe((topic) => {
      this.getConversationMessages(topic);
      if (this.hasTopic && this.webSocket) {
        console.log('disconecting');
        this.webSocket._disconnect();
      }
      this.hasTopic = true;
      console.log('topic', topic);
      this.connectConversation(topic);
      this.topic = topic;
    });
  }

  getUser() {
    this.user = this.localStorageService.get('user');
  }

  connectConversation(topic: string) {
    this.webSocket = new WebSocketService(
      topic,
      'conversation/' + topic,
      this.onMessage.bind(this),
      null
    );
    this.webSocket._connect();
  }

  getConversationMessages(conversationId: string) {
    this.messagesService.listConversationMessages(conversationId)
    .then((messages) => {
      this.messages = messages;
      this.sortMessages();

    }).catch(console.log);
  }

  sortMessages() {
    this.messages.sort((a, b) => {
      if (a.time < b.time) {
        return -1;
      } else if (a.time == b.time) {
        return 0;
      } else {
        return 1;
      }
    });
  }

  sendMessage() {
    const message = new Message(new Date(), this.messageText, this.user.name, this.user.email);
    this.webSocket._send(message, 'chat.sendMessage/'+this.topic);
    this.messageText = '';
  }

  onMessage(message: Message, topic) {
    this.messages.push(message);
    this.sortMessages();
  }

}