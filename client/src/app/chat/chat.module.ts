import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConversationsComponent } from './conversations/conversations.component';
import { MessageAreaComponent } from './message-area/message-area.component';
import { ChatScreenComponent } from './chat-screen/chat-screen.component';
import { ChatRoutingModule } from './chat-routing.module';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import { FormsModule }   from '@angular/forms';


@NgModule({
  declarations: [ConversationsComponent, MessageAreaComponent, ChatScreenComponent],
  imports: [
    CommonModule,
    ChatRoutingModule,
    MatDividerModule,
    MatListModule,
    MatButtonModule,
    MatRippleModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    ScrollingModule,
    MatExpansionModule,
    MatGridListModule,
    FormsModule,
  ]
})
export class ChatModule { }
