import { UserService } from './../../services/user.service';
import { ConversationService } from './../../services/conversation.service';
import { Conversation } from './../../model/conversation';
import { WebSocketService } from './../../services/web-socket.service';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatAccordion } from '@angular/material';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { User } from 'src/app/model/user';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.scss']
})
export class ConversationsComponent implements OnInit {
  @ViewChild(MatAccordion) 
  accordion: MatAccordion;
  @Input()
  conversationsId: string[]
  @Input()
  topicId:string;
  @Input()
  userEmail: string;
  webSocketService:WebSocketService;
  conversations:Conversation[] = [];
  emails: string[] = [];
  email: string;
  groupName: string = "";
  messagesTopics: WebSocketService[] = [];
  lastSelectedId: string;
  constructor(private conversationService: ConversationService, private userService: UserService, private localStorageService:LocalStorageService, private router: Router) {
  }

  ngOnInit() {
    this.conversationService.listConversations(this.conversationsId).then(res=>{
      this.conversations = res;
    }).catch((err:HttpErrorResponse)=>{
      console.error(err);
    }).finally(()=>{
      this.webSocketService = new WebSocketService(this.topicId, "user/"+this.topicId, this.handleReceivedConversation.bind(this));
      this.webSocketService._connect();
      this.conversations.forEach(item=>{
        this.addToMessagesTopics(item)
      });
    });
  }

  handleReceivedConversation(conversation: Conversation){
    let c: Conversation = conversation;
    this.conversations.unshift(c);
    this.conversationsId.unshift(c.id)
    this.localStorageService.addConversationId("user", c.id);
  }

  handleReceivedMessage(message: string, topicId: string){
    const index = this.conversations.findIndex((item, index)=>{
      return item.id==topicId
    })
    if (!this.conversations[index].selected){
      this.conversations[index].viewed=false;
    }
    this.conversations[index].lastUpdated=new Date();
    this.conversations.sort(this.compareDate);
  }

  addToMessagesTopics(conversation: Conversation){
    conversation.lastUpdated = new Date();
    conversation.viewed = true;
    conversation.selected = false;
    const ws = new WebSocketService(conversation.id, "conversation/"+conversation.id, this.handleReceivedMessage.bind(this));
    ws._connect();
    this.messagesTopics.push(ws);
  }

  addEmail(){
    if (!this.email){
      console.log("E-mail vazio");
      return
    }else if (this.email==this.userEmail){
      console.log("Vc não pode se adicionar :)");
      return
    }else if(this.emails.indexOf(this.email)!=-1){
      console.log("E-mail já está na lista.");
      return
    }
    this.userService.exists(this.email).then(res=>{
      if (res["exists"]){
        this.emails.unshift(this.email);
        this.email = ""
      }else{
        console.error("Esse e-mail não existe");
      }
    }).catch((err:HttpErrorResponse)=>{
      console.error(err);
    });
  }

  removeEmail(email:string){
    this.emails.splice(this.emails.indexOf(email), 1);
  }

  groupNameEditable(){
    return this.emails.length>1
  }
  
  createConversation(){
    if (this.emails.length>1 && this.groupName.length<3){
      console.log("Nome do grupo obrigatório");
      return
    }
    if (this.emails.length==0){
      console.log("Informe os emails");
      return
    }

    this.emails.unshift(this.userEmail)
    this.conversationService.createConversation(new Conversation(this.groupName, this.emails))
      .then(res=>{
        console.log(res);
      }).catch((err:HttpErrorResponse)=>{
        console.error(err);
      }).finally(()=>{
        this.groupName = "";
        this.emails = [];
        this.email = "";
        this.accordion.closeAll();
      })
  }

  getOtherParticipant(participants: string[]){
    return participants[(participants.indexOf(this.userEmail)+1)%2]
  }

  getConversationId(id: string){
    if (this.lastSelectedId){
      const indexSelectedId = this.conversations.findIndex((item, index)=>{
        return item.id==this.lastSelectedId
      })
      this.conversations[indexSelectedId].selected = false;
    }
    const index = this.conversations.findIndex((item, index)=>{
      return item.id==id
    })
    this.conversations[index].selected = true;
    this.conversations[index].viewed = true;
    this.lastSelectedId = id;
    ConversationService.emitConversationIdToRenderMessages.emit(id);
  }

  compareDate(a:Conversation, b:Conversation){
    if (a.lastUpdated < b.lastUpdated) {
      return 1;
    } else if (a.lastUpdated == b.lastUpdated) {
      return 0;
    } else {
      return -1;
    }
  }

  logout() {
    this.localStorageService.remove('user');
    this.localStorageService.remove('password');
    this.router.navigate(['']);
  }
}
